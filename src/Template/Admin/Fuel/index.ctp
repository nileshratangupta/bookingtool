<!-- Breadcrumb-->
      <div class="breadcrumb-holder">
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><?= $this->Html->link('<i class="fa fa-dashboard"></i> '.__('Home'), ['controller'=>'Users','action' => 'dashboard'], ['escape' => false]) ?></li>
            <li class="breadcrumb-item active">Listing</li>
          </ul>
        </div>
      </div>
      <section>
        <div class="container-fluid">
          <div class="row">
            <div class="pull-right"><?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
          </div>
          <!-- Page Header-->
          <header> 
            <h1 class="h3 display">List Of Fuels</h1>
          </header>
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="userListing" style="width: 100%;" class="table">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Fuel Amount</th>
                      <th>Fuel Date</th>
                      <th>Driver Name</th>
                      <th>Feul Identity Photo</th>
                      <th>Feul Receipt ID</th>
                      <th>Car Name</th>
                      <th><?= __('Actions') ?></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($fuels as $fuel): ?>
                    <tr>
                      <td><?= $this->Number->format($fuel->id) ?></td>
                      <td><?= h($fuel->fuel_amount) ?></td>
                      <td><?= h($fuel->fuel_date) ?></td>
                      <td><?= h($fuel->drivers->name) ?></td>
                      <td><img src="<?php echo WEBROOT_IMG_PATH.$fuel->fuel_photo ?>" height=100 width=100></td>
                      <td><?= h($fuel->fuel_receipt_id) ?></td>
                      <td><?= h($fuel->cars->title) ?></td>
                      <td class="actions" style="white-space:nowrap">
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $fuel->id], ['class'=>'btn btn-warning btn-xs']) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fuel->id], ['confirm' => __('Confirm to delete this entry?'), 'class'=>'btn btn-danger btn-xs']) ?>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>