<!-- Breadcrumb-->
      <div class="breadcrumb-holder">
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><?= $this->Html->link('<i class="fa fa-dashboard"></i> '.__('Home'), ['controller'=>'Users','action' => 'dashboard'], ['escape' => false]) ?></li>
            <li class="breadcrumb-item active">Listing</li>
          </ul>
        </div>
      </div>
      <section>
        <div class="container-fluid">
          <div class="row">
            <div class="pull-right"><?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
          </div>
          <!-- Page Header-->
          <header> 
            <h1 class="h3 display">List Of Cars</h1>
          </header>
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="userListing" style="width: 100%;" class="table">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Title</th>
                      <th>Site Name</th>
                      <th>Price</th>
                      <th>Price per day</th>
                      <th>Images</th>
                      <th><?= __('Actions') ?></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($cars as $car): ?>
                    <tr>
                      <td><?= $this->Number->format($car->id) ?></td>
                      <td><?= h($car->title) ?></td>
                      <td><?= h($car->sites->title) ?></td>
                      <td><?= h($car->price) ?></td>
                      <td><?= h($car->price_day) ?></td>
                      <td><img src="<?php echo WEBROOT_IMG_PATH.$car->images ?>" height=100 width=100></td>
                      <td class="actions" style="white-space:nowrap">
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $car->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $car->id], ['confirm' => __('Confirm to delete this entry?'), 'class'=>'btn btn-danger btn-xs']) ?>
                    </td>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>