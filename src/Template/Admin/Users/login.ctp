		  <div class="header"> 
            <h1 class="page-header">
                Login <small>Login Panel</small>
            </h1>
			<?php  echo $this->General->getbreadcrums($breadcrumbs); ?>		 

		</div>	
				   <div class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            <div class="logo text-uppercase"><span>Booking</span><strong class="text-primary">Tool</strong></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>

            <?= $this->Flash->render() ?>
    <?= $this->Form->create('Users',['class'=>'text-left form-validate']) ?>
      <div class="form-group-material">
        <?= $this->Form->control('email_address',['class'=>'input-material','label'=>false,'placeholder'=>'Username']) ?>
      </div>
      <div class="form-group-material">
        <?= $this->Form->control('password',['class'=>'input-material','label'=>false,'placeholder'=>'Password']) ?>
      </div>
      <div class="form-group text-center">
        <?= $this->Form->button(__('Login'),['class'=>'btn btn-primary']); ?>
      </div>
        <?= $this->Form->end() ?>
           <!-- <a href="#" class="forgot-pass">Forgot Password?</a><small>Do not have an account? </small><a href="register.html" class="signup">Signup</a> -->
          </div>
          <div class="copyrights text-center">
            <p>Design by <a href="#" class="external"><?php echo SITE_NAME ?></a>                        </p>
          </div>
        </div>
      </div>
    </div>
           
            