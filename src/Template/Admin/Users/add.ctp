<!-- Breadcrumb-->
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><?= $this->Html->link('<i class="fa fa-dashboard"></i> '.__('Home'), ['action' => 'dashboard'], ['escape' => false]) ?></li>
        <li class="breadcrumb-item active">Add User</li>
      </ul>
    </div>
  </div>

<section class="forms">
        <div class="container-fluid">
          <!-- Page Header-->
          <header> 
            <h1 class="h3 display">Add User</h1>
          </header>
          <div class="row"> 
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                <?= $this->Form->create($user) ?>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group-material">
                            <?= $this->Form->control('first_name',['class'=>'input-material','id'=>'register-firstname']) ?>
                        </div>
                        <div class="form-group-material">
                            <?= $this->Form->control('last_name',['class'=>'input-material','id'=>'register-lastname']) ?>
                        </div>
                        <div class="form-group-material">
                            <?= $this->Form->control('email_address',['class'=>'input-material']) ?>
                        </div>
                        <div class="form-group-material">
                            <?= $this->Form->control('password',['class'=>'input-material']) ?>
                        </div>
                        <div class="line"></div>
                        <div class="form-group row">
                          <div class="col-sm-12 mb-3">
                            <?= $this->Form->control('role_id', [
                                    'options' => ['1' => 'Admin', '2' => 'User'],'data-style'=>'btn-outline-dark','class'=>'form-control'
                                ]) ?>
                          </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-4 offset-sm-2">
                        <?= $this->Form->button(__('Save'),['class'=>'btn btn-primary']); ?>
                      </div>
                    </div>
                  <?= $this->Form->end() ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
