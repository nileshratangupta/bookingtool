<!-- Breadcrumb-->
      <div class="breadcrumb-holder">
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><?= $this->Html->link('<i class="fa fa-dashboard"></i> '.__('Home'), ['controller'=>'Users','action' => 'dashboard'], ['escape' => false]) ?></li>
            <li class="breadcrumb-item active">Listing</li>
          </ul>
        </div>
      </div>
      <section>
        <div class="container-fluid">
          <div class="row">
            <div class="pull-right"><?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
          </div>
          <!-- Page Header-->
          <header> 
            <h1 class="h3 display">List Of Users</h1>
          </header>
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="userListing" style="width: 100%;" class="table">
                  <thead>
                    <tr>
                      <th>id</th>
                      <th>Full Name</th>
                      <th>Email</th>
                      <th>Status</th>
                      <th>Role</th>
                      <th><?= __('Actions') ?></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($users as $user): ?>
                    <tr>
                      <td><?= $this->Number->format($user->id) ?></td>
                      <td><?= h($user->first_name).' '.h($user->last_name) ?></td>
                      <td><?= h($user->email_address) ?></td>
                      <?php if($user->status == STA_ACTIVE): ?>
                      <td><?= h('Active') ?></td>
                      <?php else: ?>
                      <td><?= h('DIsActive') ?></td>
                      <?php endif;?>

                      <?php if($user->role_id == ADMIN_ROLE): ?>
                      <td><?= h('Admin') ?></td>
                      <?php else: ?>
                      <td><?= h('User') ?></td>
                      <?php endif;?>
                      <td class="actions" style="white-space:nowrap">
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Confirm to delete this entry?'), 'class'=>'btn btn-danger btn-xs']) ?>
                    </td>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>