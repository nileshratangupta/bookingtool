<!-- Breadcrumb-->
<div class="breadcrumb-holder">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><?= $this->Html->link('<i class="fa fa-dashboard"></i> '.__('Home'), ['controller'=>'Users','action' => 'dashboard'], ['escape' => false]) ?></li>
      <li class="breadcrumb-item active">Listing</li>
    </ul>
  </div>
</div>
<section>
  <div class="container-fluid">
    <div class="row">
      <div class="pull-right"><?= $this->Html->link(__('New'), ['action' => 'edit'], ['class'=>'btn btn-success btn-xs']) ?></div>
    </div>
    <!-- Page Header-->
    <header> 
      <h1 class="h3 display">List Of Taxs</h1>
    </header>
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table id="userListing" style="width: 100%;" class="table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Website</th>
                <th>Status</th>
                <th><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($taxs as $tax): ?>
              <tr>
                <td><?= $this->Number->format($tax->id) ?></td>
                <td><?= h($tax->tax) ?></td>
                <td><?= h($tax->sites->title) ?></td>
                <?php if($tax->status == STA_ACTIVE): ?>
                <td><?= h('Active') ?></td>
                <?php else: ?>
                <td><?= h('DIsActive') ?></td>
                <?php endif;?>
                <td class="actions" style="white-space:nowrap">
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tax->id], ['class'=>'btn btn-warning btn-xs']) ?>
              </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>