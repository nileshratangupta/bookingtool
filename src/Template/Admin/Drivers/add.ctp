<!-- Breadcrumb-->
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><?= $this->Html->link('<i class="fa fa-dashboard"></i> '.__('Home'), ['controller'=>'Users','action' => 'dashboard'], ['escape' => false]) ?></li>
        <li class="breadcrumb-item active">Add Driver</li>
      </ul>
    </div>
  </div>

<section class="forms">
        <div class="container-fluid">
          <!-- Page Header-->
          <header> 
            <h1 class="h3 display">Add Driver</h1>
          </header>
          <div class="row"> 
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                <?= $this->Form->create($driver, array('role' => 'form')) ?>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group-material">
                            <?= $this->Form->control('name',['class'=>'input-material']); ?>
                        </div>
                        <div class="form-group-material">
                            <?= $this->Form->control('email',['class'=>'input-material']); ?>
                        </div>
                        <div class="form-group-material">
                            <?= $this->Form->control('password',['class'=>'input-material']); ?>
                        </div>
                        <div class="form-group-material">
                            <?= $this->Form->control('phone',['class'=>'input-material']); ?>
                        </div>
                        <div class="form-group-material">
                            <?= $this->Form->control('city',['class'=>'input-material']); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-4 offset-sm-2">
                        <?= $this->Form->button(__('Save'),['class'=>'btn btn-primary']); ?>
                      </div>
                    </div>
                  <?= $this->Form->end() ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>