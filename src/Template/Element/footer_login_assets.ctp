<script src="<?php echo SITE_URL.'admin/assets/vendor/popper.js/umd/popper.min.js' ?>"> </script>
<script src="<?php echo SITE_URL.'admin/assets/vendor/bootstrap/js/bootstrap.min.js' ?>"></script>
<script src="<?php echo SITE_URL.'admin/assets/js/grasp_mobile_progress_circle-1.0.0.min.js' ?>"></script>
<script src="<?php echo SITE_URL.'admin/assets/vendor/jquery.cookie/jquery.cookie.js' ?>"> </script>
<script src="<?php echo SITE_URL.'admin/assets/vendor/chart.js/Chart.min.js' ?>"></script>
<script src="<?php echo SITE_URL.'admin/assets/vendor/jquery-validation/jquery.validate.min.js' ?>"></script>
<script src="<?php echo SITE_URL.'admin/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js' ?>"></script>
<!-- Main File-->
<script src="<?php echo SITE_URL.'admin/assets/js/front.js' ?>"></script>