<?php echo $this->Html->script(SITE_URL.'admin/assets/vendor/popper.js/umd/popper.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/vendor/bootstrap/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/grasp_mobile_progress_circle-1.0.0.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/vendor/jquery.cookie/jquery.cookie.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/vendor/jquery-validation/jquery.validate.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/jquery.validate.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/vendor/chart.js/Chart.min.js') ?>
     
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/charts-home.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/bootstrap-select.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/jquery.dataTables.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/dataTables.bootstrap4.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/dataTables.responsive.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/jquery.validate.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/vendor/messenger-hubspot/build/js/messenger.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/vendor/messenger-hubspot/build/js/messenger-theme-flat.js') ?>
    <?php  echo $this->Html->script(SITE_URL.'admin/assets/js/custom.js') ?>
    <?php  echo $this->Html->script(SITE_URL.'admin/assets/js/summernote-bs4.min.js') ?>
    <?php  echo $this->Html->script(SITE_URL.'admin/assets/js/forms-texteditor.js') ?>
    <?php  echo $this->Html->script(SITE_URL.'admin/assets/js/dropzone.js') ?>
    <?php  echo $this->Html->script(SITE_URL.'admin/assets/js/forms-dropzone.js') ?>
    <?php  echo $this->Html->script(SITE_URL.'admin/assets/js/bootstrap-datepicker.min.js') ?>
<script type="text/javascript">
setTimeout(function(){
    $(function() {
        var tmpTextFlash = $("#flash_message_display").find('.message').text();
        var tmpTextclass = $("#flash_message_display").find('.message').prop('class');
        var tmpTextclassMessage = tmpTextclass.replace('message ','');
        
    Messenger.options = {
        extraClasses: "messenger-fixed messenger-on-top  messenger-on-right",
        theme: "flat",
        messageDefaults: {
            showCloseButton: !0
        }
    }, Messenger().post({
        message: tmpTextFlash,
        type: tmpTextclassMessage
    })
});

},1000);
        

    </script>

    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/front.js') ?>
    
    
