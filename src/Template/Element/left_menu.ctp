<nav class="side-navbar">
      <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <?php if(isset($authUser)): ?>
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <!-- User Info-->
          <div class="sidenav-header-inner text-center"><a href="<?php echo SITE_URL; ?>"><img src="<?php echo SITE_URL.'admin/assets/img/avatar-7.jpg' ?>" alt="person" class="img-fluid rounded-circle"></a>

            <h2 class="h5"><?php echo $authUser['first_name'].' '.$authUser['last_name'] ?></h2><span>Web Developer</span>
          </div>

          <!-- Small Brand information, appears on minimized sidebar-->
          <div class="sidenav-header-logo"><a href="<?php echo SITE_URL;  ?>" class="brand-small text-center"> <strong><?php echo substr($authUser['first_name'],0,1) ?></strong><strong class="text-primary"><?php echo substr($authUser['last_name'],0,1) ?></strong></a></div>
        </div>
        <?php endif;?>
        <!-- Sidebar Navigation Menus-->
         <?php /* if ($is_auth) { */ ?>
        <div class="main-menu">
          <h5 class="sidenav-heading">Main</h5>
          <ul id="side-main-menu" class="side-menu list-unstyled">   
            <li><?= $this->Html->link('<i class="icon-home"></i> '.__('Home'), ['controller'=>'Users','action' => 'dashboard'], ['escape' => false]) ?></li>
            <li><a href="#Users" aria-expanded="false" data-toggle="collapse"> <i class="icon-form"></i>Manage Users </a>
              <ul id="Users" class="collapse list-unstyled ">
                <li><?= $this->Html->link(__('All Users'), ['controller'=>'Users','action' => 'index'], ['escape' => false]) ?></li>
                <li><?= $this->Html->link(__('Add User'), ['controller'=>'Users','action' => 'add'], ['escape' => false]) ?></li>
              </ul>
            </li>
            <li><a href="#Cars" aria-expanded="false" data-toggle="collapse"> <i class="icon-form"></i>Manage Cars </a>
              <ul id="Cars" class="collapse list-unstyled ">
                <li><?= $this->Html->link(__('All Cars'), ['controller'=>'Cars','action' => 'index'], ['escape' => false]) ?></li>
                <li><?= $this->Html->link(__('Add Cars'), ['controller'=>'Cars','action' => 'add'], ['escape' => false]) ?></li>
              </ul>
            </li>
            <li><a href="#Coupons" aria-expanded="false" data-toggle="collapse"> <i class="icon-form"></i>Manage Coupons </a>
              <ul id="Coupons" class="collapse list-unstyled ">
                <li><?= $this->Html->link(__('All Coupons'), ['controller'=>'Coupons','action' => 'index'], ['escape' => false]) ?></li>
                <li><?= $this->Html->link(__('Add Coupons'), ['controller'=>'Coupons','action' => 'add'], ['escape' => false]) ?></li>
              </ul>
            </li>
            <li><a href="#Drivers" aria-expanded="false" data-toggle="collapse"> <i class="icon-form"></i>Manage Drivers </a>
              <ul id="Drivers" class="collapse list-unstyled ">
                <li><?= $this->Html->link(__('All Drivers'), ['controller'=>'Drivers','action' => 'index'], ['escape' => false]) ?></li>
                <li><?= $this->Html->link(__('Add Drivers'), ['controller'=>'Drivers','action' => 'add'], ['escape' => false]) ?></li>
              </ul>
            </li>
            <li><a href="#Driverabsents" aria-expanded="false" data-toggle="collapse"> <i class="icon-form"></i>Manage Driverabsents </a>
              <ul id="Driverabsents" class="collapse list-unstyled ">
                <li><?= $this->Html->link(__('All Driverabsents'), ['controller'=>'Driverabsents','action' => 'index'], ['escape' => false]) ?></li>
                <li><?= $this->Html->link(__('Add Driverabsents'), ['controller'=>'Driverabsents','action' => 'add'], ['escape' => false]) ?></li>
              </ul>
            </li>
            <li><a href="#Flatrates" aria-expanded="false" data-toggle="collapse"> <i class="icon-form"></i>Manage Flatrates </a>
              <ul id="Flatrates" class="collapse list-unstyled ">
                <li><?= $this->Html->link(__('All Flatrates'), ['controller'=>'Flatrates','action' => 'index'], ['escape' => false]) ?></li>
                <li><?= $this->Html->link(__('Add Flatrates'), ['controller'=>'Flatrates','action' => 'add'], ['escape' => false]) ?></li>
              </ul>
            </li>
            <li><a href="#Fuels" aria-expanded="false" data-toggle="collapse"> <i class="icon-form"></i>Manage Fuels </a>
              <ul id="Fuels" class="collapse list-unstyled ">
                <li><?= $this->Html->link(__('All Fuels'), ['controller'=>'Fuel','action' => 'index'], ['escape' => false]) ?></li>
                <li><?= $this->Html->link(__('Add Fuels'), ['controller'=>'Fuel','action' => 'add'], ['escape' => false]) ?></li>
              </ul>
            </li>
            <li><a href="#Sites" aria-expanded="false" data-toggle="collapse"> <i class="icon-form"></i>Manage Sites </a>
              <ul id="Sites" class="collapse list-unstyled ">
                <li><?= $this->Html->link(__('All Sites'), ['controller'=>'Sites','action' => 'index'], ['escape' => false]) ?></li>
                <li><?= $this->Html->link(__('Add Sites'), ['controller'=>'Sites','action' => 'add'], ['escape' => false]) ?></li>
              </ul>
            </li>
            <li><a href="#Taxs" aria-expanded="false" data-toggle="collapse"> <i class="icon-form"></i>Manage Taxs </a>
              <ul id="Taxs" class="collapse list-unstyled ">
                <li><?= $this->Html->link(__('All Taxs'), ['controller'=>'Taxs','action' => 'index'], ['escape' => false]) ?></li>
                <li><?= $this->Html->link(__('Add Taxs'), ['controller'=>'Taxs','action' => 'edit'], ['escape' => false]) ?></li>
              </ul>
            </li>
          </ul>
        </div>
        <?php //} ?>
      </div>
    </nav>