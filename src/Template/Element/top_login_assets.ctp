<link rel="stylesheet" href="<?php echo SITE_URL.'admin/assets/css/bootstrap.min.css' ?>">
<!-- Font Awesome CSS-->
<link rel="stylesheet" href="<?php echo SITE_URL.'admin/assets/css/font-awesome.min.css' ?>">
<link rel="stylesheet" href="<?php echo SITE_URL.'admin/assets/css/fontastic.css' ?>">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<link rel="stylesheet" href="<?php echo SITE_URL.'admin/assets/css/grasp_mobile_progress_circle-1.0.0.min.css' ?>">
<link rel="stylesheet" href="<?php echo SITE_URL.'admin/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css' ?>">
<link rel="stylesheet" href="<?php echo SITE_URL.'admin/assets/css/style.default.premium.css' ?>" id="theme-stylesheet">
<link rel="stylesheet" href="<?php echo SITE_URL.'admin/assets/css/custom.css' ?>">
<link rel="shortcut icon" href="https://d19m59y37dris4.cloudfront.net/dashboard-premium/1-4-4/img/favicon.ico">
<?php echo $this->Html->script(SITE_URL.'admin/assets/vendor/jquery/jquery.min.js') ?>