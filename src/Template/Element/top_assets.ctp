<?php echo $this->Html->css(SITE_URL.'admin/assets/css/bootstrap.min.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/font-awesome.min.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/fontastic.css') ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/grasp_mobile_progress_circle-1.0.0.min.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/style.default.premium.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/custom.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/bootstrap-select.min.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/dataTables.bootstrap4.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/summernote-bs4.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/bootstrap-datepicker3.css') ?>
    <link rel="shortcut icon" href="https://d19m59y37dris4.cloudfront.net/dashboard-premium/1-4-4/img/favicon.ico">
     <?php echo $this->Html->script(SITE_URL.'admin/assets/vendor/jquery/jquery.min.js') ?>
    <?php # echo $this->Html->script(SITE_URL.'admin/assets/js/jquery-1.10.2.js') ?>
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />