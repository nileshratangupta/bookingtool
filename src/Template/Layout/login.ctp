<!DOCTYPE html>
<!-- 
Template Name: BRILLIANT Bootstrap Admin Template
Version: 4.5.6
Author: WebThemez
Website: http://www.webthemez.com/ 
-->
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Mirrored from webthemez.com/demo/brilliant-free-bootstrap-admin-template/empty.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 Aug 2018 07:43:58 GMT -->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta content="" name="description" />
    <meta content="webthemez" name="author" />
    <title><?php echo $this->fetch('title'); ?> - <?php echo SITE_NAME ?></title>
	<!-- Bootstrap Styles-->
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/bootstrap.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/font-awesome.css') ?>
    <?php echo $this->Html->css(SITE_URL.'admin/assets/css/custom-styles.css') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/jquery-1.10.2.js') ?>
     <!-- Google Fonts-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <?php echo $this->element('top_nav'); ?>
        <?php echo $this->element('left_menu'); ?>
        <?php echo $this->Flash->render() ?>
        <?php echo $this->fetch('content') ?>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->

    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/bootstrap.min.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/jquery.metisMenu.js') ?>
    <?php echo $this->Html->script(SITE_URL.'admin/assets/js/custom-scripts.js') ?>
   
</body>

<!-- Mirrored from webthemez.com/demo/brilliant-free-bootstrap-admin-template/empty.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 Aug 2018 07:43:58 GMT -->
</html>