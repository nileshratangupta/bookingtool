<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $this->fetch('title'); ?> - <?php echo SITE_NAME ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
     <?php echo $this->element('top_login_assets'); ?>
  </head>
<body>
    
    <?php // echo $this->Flash->render() ?>
    <?php echo $this->fetch('content') ?>
    <?php echo $this->element('footer_login_assets'); ?>
         
        
</body>
</html>