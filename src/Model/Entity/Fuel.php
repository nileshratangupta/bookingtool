<?php
namespace App\Model\Entity;
use Cake\ORM\Entity;

/**
 * Fuel Entity
 *
 * @property int $id
 * @property float $fuel_amount
 * @property date $fuel_date
 * @property int $driver_id
 * @property text $fuel_photo
 * @property int $car_id
 * @property enum $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Fuel extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fuel_amount' => true,
        'fuel_date' => true,
        'driver_id' => true,
        'fuel_photo' => true,
        'fuel_receipt_id'=>true,
        'car_id' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
    ];
}
