<?php
namespace App\Model\Entity;
use Cake\ORM\Entity;

/**
 * Car Entity
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $short_description
 * @property int $site_id
 * @property int $passanger_capacity
 * @property int $beg_capacity
 * @property text $images
 * @property float $form_price
 * @property float $price
 * @property float $price_hourly
 * @property float $price_day
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Car extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'description' => true,
        'short_description' => true,
        'website_id' => true,
        'passenger_capacity' => true,
        'beg_capacity' => true,
        'images' => true,
        'form_price' => true,
        'price' => true,
        'price_hourly' => true,
        'price_day' => true,
        'created' => true,
        'modified' => true,
    ];
}
