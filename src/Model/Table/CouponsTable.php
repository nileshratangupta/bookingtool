<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class CouponsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('coupon_code');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Sites',[
                'className'=>'Sites',
                'foreignKey'=>'website_id',
                'propertyName'=>'sites',
        ]);
    }
    
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('coupon_code')
            ->maxLength('coupon_code', 255)
            ->requirePresence('coupon_code', 'create')
            ->notEmpty('coupon_code');

        $validator
            ->scalar('percentage')
            ->requirePresence('percentage', 'create')
            ->notEmpty('percentage')
            ->numeric('percentage');

        $validator
            ->scalar('start_date')
            ->requirePresence('start_date', 'create')
            ->notEmpty('start_date');
        $validator
            ->scalar('end_date')
            ->requirePresence('end_date', 'create')
            ->notEmpty('end_date');
        $validator
            ->scalar('website_id')
            ->requirePresence('website_id', 'create')
            ->notEmpty('website_id');

        return $validator;
    }
   
}
