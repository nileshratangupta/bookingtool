<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class TaxsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tax');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Sites',[
                'className'=>'Sites',
                'foreignKey'=>'website_id',
                'propertyName'=>'sites',
        ]);

    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('tax')
            ->maxLength('tax', 255)
            ->requirePresence('tax', 'create')
            ->notEmpty('tax')
            ->numeric('tax');

        $validator
            ->scalar('website_id')
            ->maxLength('website_id', 10)
            ->requirePresence('website_id', 'create')
            ->notEmpty('website_id');

        return $validator;
    }

}
