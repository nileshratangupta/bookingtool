<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class FlatratesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('flat_trip');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }
    
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('from_name')
            ->maxLength('from_name', 255)
            ->requirePresence('from_name', 'create')
            ->notEmpty('from_name');

        $validator
            ->scalar('to_name')
            ->maxLength('to_name', 255)
            ->requirePresence('to_name', 'create')
            ->notEmpty('to_name');

        return $validator;
    }
   
}
