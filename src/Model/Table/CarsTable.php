<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cars Model
 *
 * @method \App\Model\Entity\Car get($primaryKey, $options = [])
 * @method \App\Model\Entity\Car newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Car[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Car|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Car|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Car patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Car[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Car findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CarsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cars');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        /*$this->addBehavior('car_images/Upload.Upload', [
            'images' => []
        ]);*/
        $this->belongsTo('Sites',[
                'className'=>'Sites',
                'foreignKey'=>'website_id',
                'propertyName'=>'sites',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->scalar('short_description')
            ->maxLength('short_description', 255)
            ->requirePresence('short_description', 'create')
            ->notEmpty('short_description');

        $validator
            ->integer('website_id')
            ->requirePresence('website_id', 'create')
            ->notEmpty('website_id', 'create');

        $validator
            ->integer('passenger_capacity')
            ->requirePresence('passenger_capacity', 'create')
            ->notEmpty('passenger_capacity', 'create')
            ->numeric('passenger_capacity');

        $validator
            ->integer('beg_capacity')
            ->requirePresence('beg_capacity', 'create')
            ->notEmpty('beg_capacity', 'create')
            ->numeric('beg_capacity');

        /*$validator
            ->scalar('images')
            ->maxLength('images', 255)
            ->requirePresence('images', 'create')
            ->notEmpty('images', 'create');

        /*$validator
            ->allowEmpty('images')
            ->add('images', [
                'validExtension' => [
                    'rule' => ['extension',['png','jpg','jpeg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                    'message' => ('These files extension are allowed: .png , .jpg , .jpeg')
                ]
        ]);*/

        $validator
            ->integer('form_price')
            ->requirePresence('form_price', 'create')
            ->notEmpty('form_price', 'create')
            ->numeric('form_price');

        $validator
            ->integer('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price', 'create')
            ->numeric('price');

        $validator
            ->integer('price_hourly')
            ->requirePresence('price_hourly', 'create')
            ->notEmpty('price_hourly', 'create')
            ->numeric('price_hourly');

        $validator
            ->integer('price_day')
            ->requirePresence('price_day', 'create')
            ->notEmpty('price_day', 'create')
            ->numeric('price_day');


        return $validator;
    }

    public function getAllCars(){
        $cars = $this->find('all');
        $car_name = [];
        $car_name[''] = 'Select car';
        foreach ($cars as $cs) {
            $car_name[$cs['id']] = $cs['title'];
        }
        return $car_name;
    }

}
