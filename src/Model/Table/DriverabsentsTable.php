<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Driverabsents Model
 *
 * @method \App\Model\Entity\Driverabsents get($primaryKey, $options = [])
 * @method \App\Model\Entity\Driverabsents newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Driverabsents[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Driverabsents|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Driverabsents|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Driverabsents patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Driverabsents[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Driverabsents findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DriverabsentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('driver_absents');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Drivers',[
                'className'=>'Drivers',
                'foreignKey'=>'driver_id',
                'propertyName'=>'drivers',
        ]);
    }


    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('driver_id')
            ->requirePresence('driver_id', 'create')
            ->notEmpty('driver_id', 'create');

        $validator
            ->scalar('avail_date')
            ->maxLength('avail_date', 255)
            ->requirePresence('avail_date', 'create')
            ->notEmpty('avail_date');

        $validator
            ->scalar('reason')
            ->maxLength('reason', 255)
            ->requirePresence('reason', 'create')
            ->notEmpty('reason');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }
}
