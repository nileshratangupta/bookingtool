<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class SitesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sites');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Cars',[
                'className'=>'Cars'
        ]);

        $this->hasMany('Coupons',[
                'className'=>'Coupons'
        ]);

        $this->hasMany('Taxs',[
                'className'=>'Taxs'
        ]);
    }
    
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');


        $validator
            ->scalar('website')
            ->maxLength('website', 255)
            ->requirePresence('website', 'create')
            ->notEmpty('website');

        return $validator;
    }
    public function getAllSites(){
        $sites = $this->find('all');
        $site_name = [];
        $site_name[''] = 'Select Website';
        foreach ($sites as $sv) {
            $site_name[$sv['id']] = $sv['title'];
        }
        return $site_name;
    }
}
