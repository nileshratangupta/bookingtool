<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fuel Model
 *
 * @method \App\Model\Entity\Fuel get($primaryKey, $options = [])
 * @method \App\Model\Entity\Fuel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Fuel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Fuel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fuel|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fuel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Fuel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Fuel findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FuelTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fuel');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Drivers',[
                'className'=>'Drivers',
                'foreignKey'=>'driver_id',
                'propertyName'=>'drivers',
        ]);

        $this->belongsTo('Cars',[
                'className'=>'Cars',
                'foreignKey'=>'car_id',
                'propertyName'=>'cars',
        ]);
    }


    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('driver_id')
            ->requirePresence('driver_id', 'create')
            ->notEmpty('driver_id', 'create');

        $validator
            ->integer('car_id')
            ->requirePresence('car_id', 'create')
            ->notEmpty('car_id', 'create');

        $validator
            ->scalar('fuel_receipt_id')
            ->maxLength('fuel_receipt_id', 255)
            ->requirePresence('fuel_receipt_id', 'create')
            ->notEmpty('fuel_receipt_id');

        $validator
            ->integer('fuel_amount')
            ->requirePresence('fuel_amount', 'create')
            ->notEmpty('fuel_amount', 'create');


        /*$validator
            ->scalar('fuel_photo')
            ->maxLength('fuel_photo', 255)
            ->requirePresence('fuel_photo', 'create')
            ->notEmpty('fuel_photo', 'create');*/

        $validator
            ->scalar('fuel_date')
            ->maxLength('fuel_date', 255)
            ->requirePresence('fuel_date', 'create')
            ->notEmpty('fuel_date');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }
}
