<?php
namespace App\View\Helper;
 
use Cake\View\Helper;
 
class GeneralHelper extends Helper {
 
 
     
    public function getbreadcrums($array = [])
    {       
    		$breadcrumb = '';

    		if(isset($array) && sizeof($array)) {
    			$breadcrumb.='<ol class="breadcrumb" >';
    			foreach($array as $ar) {
    				if(isset($ar['title'])) {
    					$url = isset($ar['url']) ? $ar['url'] : '';
    					$breadcrumb.='<li><a href="'.$url.'">'.$ar['title'].'</a></li>';
    				}
    				
    			}
    			$breadcrumb.='</ol>';
    		}
        	
        return $breadcrumb;
    }
}
?>