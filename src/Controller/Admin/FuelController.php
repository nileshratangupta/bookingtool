<?php
namespace App\Controller\Admin;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * Fuel Controller
 *
 * @property \App\Model\Table\CarsTable $Fuel
 *
 * @method \App\Model\Entity\car[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FuelController extends AppController
{

    public function initialize()
    {
         parent::initialize();
         $this->loadComponent('Flash');
         $this->layout = 'admin_layout';
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    var $components = array('General');

    public function index()
    {
        //$fuels = $this->paginate($this->Fuel);
        $fuels = $this->Fuel->find()->contain([
            'Drivers','Cars'
        ]);
        //echo "<pre>";print_r($fuels);die;
        $this->set(compact('fuels'));
    }

    /**
     * View method
     *
     * @param string|null $id fuel id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fuel = $this->Fuel->get($id, [
            'contain' => ['Drivers','Cars']
        ]);

        $this->set('fuel', $fuel);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fuel = $this->Fuel->newEntity();
        if ($this->request->is('post')) {
            
            /*$day = $this->request->data['fuel_date']['day'];
            $month = $this->request->data['fuel_date']['month'];
            $year = $this->request->data['fuel_date']['year'];
            
            $dateFormate = $day.'/'.$month.'/'.$year;

            $this->request->data['fuel_date'] = $dateFormate;*/
            //print_r($this->request->data); die;
            if(isset($this->request->data['fuel_photo']['size']) && $this->request->data['fuel_photo']['size'] > 0) {
                $image = isset($this->request->data['fuel_photo']) ? $this->request->data['fuel_photo'] : [];
                //pr($image);die;
                //echo FUEL_IMAGES; die;
                $return_path = '';
                $return_path = $this->General->upload_files_to_server($image,FUEL_IMAGES);
               // / pr($return_path);die;
                if($return_path != '') {
                    $this->request->data['fuel_photo'] = $return_path;   
               }
            }
            $fuel = $this->Fuel->patchEntity($fuel, $this->request->data);
            if ($this->Fuel->save($fuel)) {
                $this->Flash->success(__('The {0} has been saved.', 'fuel'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'fuel'));
            }
        }

        $this->Drivers = TableRegistry::get('Drivers');
        $allDrivers = $this->Drivers->getAllDrivers();

        $cars = TableRegistry::get('Cars');
        $allCars = $cars->getAllCars();
    
        $this->set(compact('fuel','allDrivers','allCars'));
        $this->set('_serialize', ['fuel']);
    }


    /**
     * Edit method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function edit($id = null)
    {
        $fuel = $this->Fuel->get($id, [
            'contain' => []
        ]);
        $db_photo = $fuel['fuel_photo'];

        if ($this->request->is(['patch', 'post', 'put'])) {
            
            /*$day = $this->request->data['fuel_date']['day'];
            $month = $this->request->data['fuel_date']['month'];
            $year = $this->request->data['fuel_date']['year'];
            
            $dateFormate = $day.'/'.$month.'/'.$year;

            $this->request->data['fuel_date'] = $dateFormate;*/
            //print_r($this->request->data); die;
            if(isset($this->request->data['fuel_photo']['size']) && $this->request->data['fuel_photo']['size'] > 0) {
                $image = isset($this->request->data['fuel_photo']) ? $this->request->data['fuel_photo'] : [];
                //pr($image);die;
                //echo FUEL_IMAGES; die;
                $return_path = '';
                $return_path = $this->General->upload_files_to_server($image,FUEL_IMAGES);
               // / pr($return_path);die;
                if($return_path != '') {
                    $this->request->data['fuel_photo'] = $return_path;   
               }
            } else{
                $this->request->data['fuel_photo'] = $db_photo;
            }
            $fuel = $this->Fuel->patchEntity($fuel, $this->request->data);
            if ($this->Fuel->save($fuel)) {
                $this->Flash->success(__('The {0} has been saved.', 'fuel'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'fuel'));
            }
        }

        $this->Drivers = TableRegistry::get('Drivers');
        $allDrivers = $this->Drivers->getAllDrivers();

        $cars = TableRegistry::get('Cars');
        $allCars = $cars->getAllCars();
    
        $this->set(compact('fuel','allDrivers','allCars'));
        $this->set('_serialize', ['fuel']);
    }

    /**
     * Delete method
     *
     * @param string|null $id fuel id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fuel = $this->Fuel->get($id);
        if ($this->Fuel->delete($fuel)) {
            $this->Flash->success(__('The {0} has been deleted.', 'fuel'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'fuel'));
        }
        return $this->redirect(['action' => 'index']);
    }
}


