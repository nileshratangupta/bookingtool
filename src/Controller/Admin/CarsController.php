<?php
namespace App\Controller\Admin;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * Cars Controller
 *
 * @property \App\Model\Table\CarsTable $Cars
 *
 * @method \App\Model\Entity\car[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CarsController extends AppController
{


    public function initialize()
    {
         parent::initialize();
         $this->loadComponent('Flash');
         $this->layout = 'admin_layout';
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    var $components = array('General');

    public function index()
    {   

        $cars = $this->Cars->find()->contain([
            'Sites'
        ]);
        $this->set(compact('cars'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $car = $this->Cars->newEntity();

        if ($this->request->is('post')) {
            //pr($this->request); die;
            if(isset($this->request->data['images']['size']) && $this->request->data['images']['size'] > 0) {
                    $image = isset($this->request->data['images']) ? $this->request->data['images'] : [];
                    //pr($image);die;
                    $return_path = '';
                    $return_path = $this->General->upload_files_to_server($image,CAR_IMAGES);
                    //pr($return_path);die;
                    if($return_path != '') {
                        $this->request->data['images'] = $return_path;
                    }
            }
            $car = $this->Cars->patchEntity($car, $this->request->data);
            // pr($car);
            if ($this->Cars->save($car)) {
                $this->Flash->success(__('Record has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Record could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('car'));
        $this->set('_serialize', ['car']);
        $this->Sites = TableRegistry::get('Sites');
        $all_site = $this->Sites->getAllSites();
        $this->set('sites',$all_site);

    } //end of function
    public function edit($id = null)
    {
        $car = $this->Cars->get($id);
        $db_photo = $car['images'];
        //pr($this->request);die;
        
        if($this->request->is(['patch', 'post', 'put'])) {
        //pr($car);die;
            if(isset($this->request->data['images']['size']) && $this->request->data['images']['size'] > 0) {
                    $image = isset($this->request->data['images']) ? $this->request->data['images'] : [];
                    //pr($image);die;
                    $return_path = '';
                    $return_path = $this->General->upload_files_to_server($image,CAR_IMAGES);
                   // pr($return_path);die;
                    if($return_path != '') {
                        $this->request->data['images'] = $return_path;
  
                   }
            } else{
                $this->request->data['images'] = $db_photo;
            }

            $car = $this->Cars->patchEntity($car, $this->request->data);
            
            if ($this->Cars->save($car)) { 
                
                $this->Flash->success('Record has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Record could not be saved. Please, try again.');
            }
        }
     
        $this->Sites = TableRegistry::get('Sites');
        $all_site = $this->Sites->getAllSites();
        $this->set('sites',$all_site);
        $this->set('car',$car);
        $this->set('_serialize', ['car']);
    } //end of function

    public function delete($id = null){

        $this->request->allowMethod(['post','delete']);
        $car = $this->Cars->get($id);
        if ($this->Cars->delete($car)) {
            $this->Flash->success('The Site has been deleted.');
            return $this->redirect(['action' => 'index']);
        }
    } //end of function
}