<?php
namespace App\Controller\Admin;

use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TaxsController extends AppController
{

    public function initialize()
    {
         parent::initialize();
         $this->loadComponent('Flash');
         $this->layout = 'admin_layout';
    }

   
    public function index()
    {
        $taxs = $this->Taxs->find()->contain([
            'Sites'
        ]);
        $this->set(compact('taxs',$this->paginate($taxs)));
    } //end of function

    /*public function add()
    {
        $this->layout = 'adminlayout';

        $taxs = $this->Taxs->newEntity();

        if ($this->request->is('post')) {
            //pr($this->request->data);die;
            $taxs = $this->Taxs->patchEntity($taxs, $this->request->data);
           // print_r($user); 
            if ($this->Taxs->save($taxs)) {
                $this->Flash->success(__('Record has been saved.', 'User'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Record could not be saved. Please, try again.'));
            }
        }
      //  $this->Sites = ClassRegistry::init('Sites');
        $this->Sites = TableRegistry::get('Sites');
        $all_site = $this->Sites->getAllSites();
       // pr($all_site);die;
        $this->set(compact('taxs'));
        $this->set('_serialize', ['taxs']);
        $this->set('sites',$all_site);
    } //end of function*/
    public function edit($id = null)
    {
        if(isset($id) && $id !== ''):
            $taxs = $this->Taxs->get($id, [
                'contain' => []
            ]);
        endif;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $website_id = isset($this->request->data['website_id']) ? $this->request->data['website_id'] : 0;
            
            $web_id = $this->Taxs->find()->hydrate(false)->where(['website_id' => $website_id])->first();
           
            if($web_id['id']> 0){
               
                $taxs = $this->Taxs->get($web_id['id']);
                //pr($taxs); die;
                $taxs = $this->Taxs->patchEntity($taxs, $this->request->data);

                    if ($this->Taxs->save($taxs)) {
                        $this->Flash->success(__('Record has been Updated.', 'User'));
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('Record could not be Updated. Please, try again.'));
                    }
                 
            }else{
                
                $taxs = $this->Taxs->newEntity();
                $taxs = $this->Taxs->patchEntity($taxs, $this->request->data);
                    if ($this->Taxs->save($taxs)) {
                        $this->Flash->success(__('Record has been saved.', 'User'));
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('Record could not be saved. Please, try again.'));
                    }
            }
            //pr($web_data);die;
            //pr($this->request->data);die;
        }
        
        $this->set('tax',$taxs);
        $this->set('_serialize', ['tax']);
        $this->Sites = TableRegistry::get('Sites');
        $all_site = $this->Sites->getAllSites();
        $this->set('sites',$all_site);
    } //end of function
    /*public function delete($id = null){

        $this->request->allowMethod(['post','delete']);
        $taxs = $this->Taxs->get($id);
        if ($this->Taxs->delete($taxs)) {
            $this->Flash->success('The Taxs has been deleted.');
            return $this->redirect(['action' => 'index']);
        }
    } //end of function*/
   
}
