<?php
namespace App\Controller\Admin;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * Driverabsents Controller
 *
 * @property \App\Model\Table\AvailabilityTable $driverabsents
 *
 * @method \App\Model\Entity\driverabsents[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DriverabsentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function initialize()
    {
         parent::initialize();
         $this->loadComponent('Flash');
         $this->layout = 'admin_layout';
    }

    public function index()
    {   
        $driverabsents = $this->Driverabsents->find()->contain([
            'Drivers'
        ]);
        $this->set('driverabsents', $driverabsents);
    }

    /**
     * View method
     *
     * @param string|null $id fuel id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $driverabsents = $this->Driverabsents->get($id, [
            'contain' => ['Drivers']
        ]);

        $this->set('driverabsents', $driverabsents);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $driverabsents = $this->Driverabsents->newEntity();
        if ($this->request->is('post')) {
        
            $driverabsents = $this->Driverabsents->patchEntity($driverabsents, $this->request->data);
            if ($this->Driverabsents->save($driverabsents)) {
                $this->Flash->success(__('The {0} has been saved.', 'driverabsents'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'driverabsents'));
            }
        }

        $this->Drivers = TableRegistry::get('Drivers');
        $allDrivers = $this->Drivers->getAllDrivers();
    
        $this->set(compact('driverabsents','allDrivers'));
        $this->set('_serialize', ['driverabsents']);
    }

    /**
     * Edit method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function edit($id = null)
    {
        $driverabsents = $this->Driverabsents->get($id, [
            'contain' => []
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $driverabsents = $this->Driverabsents->patchEntity($driverabsents, $this->request->data);
            if ($this->Driverabsents->save($driverabsents)) {
                $this->Flash->success(__('The {0} has been saved.', 'driverabsents'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'driverabsents'));
            }
        }

        $this->Drivers = TableRegistry::get('Drivers');
        $allDrivers = $this->Drivers->getAllDrivers();

        $this->set(compact('driverabsents','allDrivers'));
        $this->set('_serialize', ['driverabsents']);
    }

    /**
     * Delete method
     *
     * @param string|null $id driverabsents id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $driverabsents = $this->Driverabsents->get($id);
        if ($this->Driverabsents->delete($driverabsents)) {
            $this->Flash->success(__('The {0} has been deleted.', 'driverabsents'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'driverabsents'));
        }
        return $this->redirect(['action' => 'index']);
    }
}


