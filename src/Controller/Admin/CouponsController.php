<?php
namespace App\Controller\Admin;

use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CouponsController extends AppController
{
   
    public function initialize()
    {
         parent::initialize();
         $this->loadComponent('Flash');
         $this->layout = 'admin_layout';
    }

    public $paginate = [
        // Other keys here.
        'maxLimit' => 10
    ];

    public function index()
    {
        $this->paginate = [
            'contain' => ['Sites']
        ];

        $this->set('coupons', $this->paginate($this->Coupons));
    }

    public function add()
    {
        $coupons = $this->Coupons->newEntity();

        if ($this->request->is('post')) {
            //pr($this->request->data);die;
            $coupons = $this->Coupons->patchEntity($coupons, $this->request->data);
           // print_r($user); 
            if ($this->Coupons->save($coupons)) {
                $this->Flash->success(__('Record has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Record could not be saved. Please, try again.'));
            }
        }
        $this->Sites = TableRegistry::get('Sites');
        $all_site = $this->Sites->getAllSites();
        $this->set('sites',$all_site);
        $this->set(compact('coupons'));
        $this->set('_serialize', ['coupons']);
    } //end of function
    public function edit($id = null)
    {
        $coupons = $this->Coupons->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $coupons = $this->Coupons->patchEntity($coupons, $this->request->data);
            if ($this->Coupons->save($coupons)) {
                $this->Flash->success(__('Record has been Updated.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Record could not be Updated. Please, try again.'));
            }
        }

        $this->Sites = TableRegistry::get('Sites');
        $all_site = $this->Sites->getAllSites();
        $this->set('sites',$all_site);
        $this->set('coupons',$coupons);
        $this->set('_serialize', ['coupons']);
        
    } //end of function
    public function delete($id = null){

        $this->request->allowMethod(['post','delete']);
        $all_site = $this->Coupons->get($id);
        if ($this->Coupons->delete($all_site)) {
            $this->Flash->success('The Coupons has been deleted.');
            return $this->redirect(['action' => 'index']);
        }
    } //end of function
   
}
