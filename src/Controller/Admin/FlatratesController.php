<?php
namespace App\Controller\Admin;

//use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FlatratesController extends AppController
{

    public function initialize()
    {
         parent::initialize();
         $this->loadComponent('Flash');
         $this->layout = 'admin_layout';
    }

    public function index()
    {
        $flat_rate = $this->paginate($this->Flatrates);

        $this->set(compact('flat_rate'));
    } //end of function

    public function add()
    {

        $flat_rate = $this->Flatrates->newEntity();

        if ($this->request->is('post')) {
            //pr($this->request->data);die;
            $flat_rate = $this->Flatrates->patchEntity($flat_rate, $this->request->data);
           // print_r($user); 
            if ($this->Flatrates->save($flat_rate)) {
                $this->Flash->success(__('Record has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Record could not be saved. Please, try again.'));
            }
        }
       
        $this->set(compact('flat_rate'));
        $this->set('_serialize', ['flat_rate']);
    } //end of function
    public function edit($id = null)
    {
        $flat_rate = $this->Flatrates->get($id);

        if ($this->request->is(['post','put'])) {
            //pr($this->request->data);die;
            $flat_rate = $this->Flatrates->patchEntity($flat_rate, $this->request->data);
           // print_r($user); 
            if ($this->Flatrates->save($flat_rate)) {
                $this->Flash->success(__('Record has been Updated.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Record could not be Updated. Please, try again.'));
            }
        }
        $this->set('flat_rate',$flat_rate);
        $this->set('_serialize', ['flat_rate']);
    } //end of function
    public function delete($id = null){

        $this->request->allowMethod(['post','delete']);
        $flat_rate = $this->Flatrates->get($id);
        if ($this->Flatrates->delete($flat_rate)) {
            $this->Flash->success('The Flat Rate has been deleted.');
            return $this->redirect(['action' => 'index']);
        }
    } //end of function
   
}
