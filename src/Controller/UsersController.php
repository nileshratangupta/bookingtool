<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException; 

class UsersController extends AppController
{	
	 public function beforeFilter(Event $event) {
	    parent::beforeFilter($event);
        $this->Auth->allow('add');
    }

    public function login()
    {
    	if($this->Auth->user()) {
    		$this->redirect($this->Auth->redirectUrl());
    	}

        $this->layout  = 'admin_login';

        $breadcrumbs = [
                ['title'=>'Home','url'=>SITE_URL],
                ['title'=>'Login','url'=>SITE_URL],
        ];

        if ($this->request->is('post')) {
        $user = $this->Auth->identify();
        //pr($user);die;

        if ($user != false) {

            $this->Auth->setUser($user);
            return $this->redirect($this->Auth->redirectUrl());
        }
        $this->Flash->error('Your username or password is incorrect.');
    }

        $this->set('breadcrumbs',$breadcrumbs);

    } // end of function

     public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    
}