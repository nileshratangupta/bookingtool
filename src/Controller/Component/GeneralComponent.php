<?php 
namespace App\Controller\Component;

use Cake\Controller\Component;

class GeneralComponent extends Component
{
    public function doComplexOperation($amount1, $amount2)
    {
        return $amount1 + $amount2;
    }

    public function upload_files_to_server($image=[],$folder = UPLOAD_IMAGE) {

	    $id=0;
	    $success  = false;
	    $uniqid   = $id.uniqid();
	    $imginfo  =  pathinfo($image['name']);
	    $ext      = $imginfo['extension'];
	    $filename = $uniqid.'.'.$ext;
	    if(move_uploaded_file($image['tmp_name'], UPLOADURL_DIR.$folder.$filename)){
	          	$path_upload = $folder.$filename;

	          	$success = true;         
	        }
	    
	    if($success) { 
	        return $path_upload;
	    }else{
	        return false;
	    }
	    
	}
}
?>