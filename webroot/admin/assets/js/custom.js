$(document).ready(function(){
  $('#userListing').DataTable();
});

jQuery(document).ready(function(){
      var date_input=jQuery('input[id="date"]'); //our date input has the name "date"
      var container=jQuery('.bootstrap-iso form').length>0 ? jQuery('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);

      jQuery("#startDate").datepicker({
          todayBtn:  1,
          autoclose: true,
          format: 'dd/mm/yyyy',
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          jQuery('#endDate').datepicker('setStartDate', minDate);
      });

      jQuery("#endDate").datepicker({
          todayBtn:  1,
          autoclose: true,
          format: 'dd/mm/yyyy',
      }).on('changeDate', function (selected) {
          var maxDate = new Date(selected.date.valueOf());
          jQuery('#startDate').datepicker('setEndDate', maxDate);
      });
      
});

